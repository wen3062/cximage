## 说明
本项目中的代码，源代码都来自网络。
 _修改了cmake 的打包CMakeLists.txt，便于在windows和centos7下都能打包成功。_ 

## windows下的编译

### 1、编译zlib

使用cmake-gui进行构建

源代码路径：E:/cximage/cximage/3rdparty/zlib
编译目标路径：E:/cximage/cximage/3rdparty/zlib/build

cmake-gui打开，点击configure，继续

修改参数：
~~~
CMAKE_CONFIGURATION_TYPES Release

CMAKE_INSTALL_PREFIX E:/cximage/cximage/3rdparty/install/zlib

INSTALL_BIN_DIR E:/cximage/cximage/3rdparty/install/zlib/bin
INSTALL_INC_DIR E:/cximage/cximage/3rdparty/install/zlib/include
INSTALL_LIB_DIR E:/cximage/cximage/3rdparty/install/zlib/lib
INSTALL_MAN_DIR E:/cximage/cximage/3rdparty/install/zlib/share/man
INSTALL_PKGCONFIG_DIR E:/cximage/cximage/3rdparty/install/zlib/share/pkgconfig
~~~

再次点击 configure，然后点击Generate，完成生成。

点击Open Project，打开vs2013开发工具。

右键每个项目，配置属性->常规，设置：

平台工具集：Visual Studio 2013 - Windows XP (v120_xp)

右键“INSTALL”项目，点击“生成”，等待生成完毕。

最终的相关文件，生成的位置为：
~~~
E:/cximage/cximage/3rdparty/install/zlib
~~~

### 2、编译libpng

使用cmake-gui进行构建

源代码路径：E:/cximage/cximage/3rdparty/libpng
编译目标路径：E:/cximage/cximage/3rdparty/libpng/build

cmake-gui打开，点击configure，继续

点击 “Finish”后，会报错误，因为需要zlib，修改参数：
~~~
CMAKE_CONFIGURATION_TYPES Release

CMAKE_INSTALL_PREFIX E:/cximage/cximage/3rdparty/install/libpng

ZLIB_INCLUDE_DIR E:/cximage/cximage/3rdparty/install/zlib/include
ZLIB_LIBRARY_RELEASE E:/cximage/cximage/3rdparty/install/zlib//lib/zlibstatic.lib
~~~

点击“Configure”，然后，点击“Generate”，

点击Open Project，打开vs2013开发工具。

切换编译类型为：Release

右键每个项目，配置属性->常规，设置：
~~~
平台工具集：Visual Studio 2013 - Windows XP (v120_xp)
~~~

右键“INSTALL”项目，点击“生成”，等待生成完毕。

最终的相关文件，生成的位置为：
~~~
E:/cximage/cximage/3rdparty/install/libpng
~~~

### 3，编译cximage

使用cmake-gui进行构建

源代码路径：E:/cximage/cximage
编译目标路径：E:/cximage/cximage/build

cmake-gui打开，点击configure，继续

点击 “Finish”后，修改参数：
~~~
CMAKE_CONFIGURATION_TYPES Release

CMAKE_INSTALL_PREFIX E:/cximage/cximage/build/install
~~~

点击“Configure”，然后，点击“Generate”，

点击Open Project，打开vs2013开发工具。

切换编译类型为：Release

右键每个项目，配置属性->常规，设置：
~~~
平台工具集：Visual Studio 2013 - Windows XP (v120_xp)
~~~

右键“INSTALL”项目，点击“生成”，等待生成完毕。

最终的相关文件，生成的位置为：
~~~
E:/cximage/cximage/build/install
~~~
拷贝相关文件到目标位置，即可使用。

## CentOS7 下的编译

使用cmake 3.20 以上版本进行构建

拷贝代码到路径下：
~~~
cd /data/cximage/cximage
~~~
### 1、编译zlib
~~~
cd /data/cximage/cximage
mkdir -p 3rdparty/zlib/build
cd 3rdparty/zlib/build

cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/data/cximage/cximage/3rdparty/install/zlib ..
make && make install
~~~
最终的相关文件，生成的位置为：
~~~
/data/cximage/cximage/3rdparty/install/zlib
~~~
### 2、编译libpng
~~~
cd /data/cximage/cximage
mkdir -p 3rdparty/libpng/build
cd 3rdparty/libpng/build

cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/data/cximage/cximage/3rdparty/install/libpng -D ZLIB_INCLUDE_DIR=/data/cximage/cximage/3rdparty/install/zlib/include -D ZLIB_LIBRARY=/data/cximage/cximage/3rdparty/install/zlib/lib/libz.a ..

make && make install
~~~

最终的相关文件，生成的位置为：
~~~
/data/cximage/cximage/3rdparty/install/libpng
~~~

### 3，编译cximage
~~~
cd /data/cximage/cximage
mkdir build
cd build

cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=./install ..
make && make install
~~~
最终的相关文件，生成的位置为：
~~~
/data/cximage/cximage/build/install
~~~

拷贝相关文件到目标位置，即可使用。